import React, { useState, useRef } from 'react'
import { createApi } from 'unsplash-js';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import StackGrid from "react-stack-grid";
import Modal from "./Modal";
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import './SearchPhotos.css'

function SearchPhotos() {

    const [query, setQuery] = useState("");
    const [photos, setPhotos] = useState([]);
    const [dataImage, setDataImage] = useState({});
    const [page, setPage] = useState(1);
    const modal = useRef(null);

    const unsplash = createApi({
        accessKey: ''
    });


    const handleScroll = (e) => {
        const bottom = (e.target.scrollHeight - e.target.scrollTop) - e.target.clientHeight <= 0.25;
        if (bottom) {
            renderNextPhotos(e);
        }
    }

    const searchPhotos = async (e) => {
        console.log(query);
        e.preventDefault();
        unsplash.search.getPhotos({
            query: query,
            page: page,
            perPage: 100
        }).then(response => response.response).then(result => {
            setPhotos(result.results);
        });
        setPage(1);
    };

    const renderNextPhotos = async (e) => {
        e.preventDefault();
        unsplash.search.getPhotos({
            query: query,
            page: page + 1,
            perPage: 100
        }).then(response => response.response).then(result => {
            setPhotos([...photos, ...result.results])
        });
        setPage(page + 1);
    }

    const clickedPhoto = (index) => {
        setDataImage({
            index: index,
            id: photos[index].id,
            created_at: photos[index].created_at,
            image: photos[index].urls.full,
            first_name: photos[index].user.first_name,
            last_name: photos[index].user.last_name,
            profile_image: photos[index].user.profile_image.medium,
            location: "location" in photos[index] ? photos[index].location.name : '',
            description: "description" in photos[index] ? photos[index].description : "Picture"
        })
        modal.current.open();
    }

    return (
        <div className="searchPhoto">
            <div className="searchbar">
                <form className="form" onSubmit={searchPhotos}>
                    <CameraAltIcon style={{ fontSize: 40 }} />
                    <input
                        type="text"
                        name="query"
                        className="input"
                        placeholder={`Try "dog" or "apple"`}
                        value={query}
                        onChange={(e) => setQuery(e.target.value)}
                    />
                    <button type="submit" className="button">
                        Search
            </button>
                </form>
            </div>
            <div onScroll={handleScroll} style={{ overflowY: 'scroll', maxHeight: '1000px' }}
            >
                <StackGrid
                    columnWidth={600}
                    monitorImagesLoaded={true}
                >
                    {photos.map((photo, i) =>
                        <img
                            alt={photo.description}
                            src={photo.urls.full}
                            width="50%"
                            height="50%"
                            key={i}
                            onClick={() => clickedPhoto(i)}
                        />
                    )}
                </StackGrid>
                <BottomScrollListener onBottom={renderNextPhotos}>
                    <div />
                </BottomScrollListener>
            </div>
            <Modal ref={modal}>
                <div className="card-modal-header">
                    <img alt="User Avatar" src={dataImage.profile_image} />
                    <span>{dataImage.first_name} {dataImage.last_name}</span>
                </div>
                <div className="card-modal-image">
                    <img src={dataImage.image} alt={dataImage.description} />
                </div>
                <div className="card-modal-footer">
                    {dataImage.location ? <><LocationOnIcon style={{ fontSize: 40 }} /> <span>{dataImage.location}</span></>
                        : null}
                </div>
            </Modal>
        </div >
    );
}

export default SearchPhotos;