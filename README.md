# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

Install all dependencies to correctly run application. 

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `unsplash API`
Due to unsplash documentation Access Key, which should remain confidential and is needed to access API,
was deleted from src/SearchPhotos.js line 19 - accessKey: ''. 
Provide your own access Key, which you can get after creating account on Unsplash and create your developer first project.